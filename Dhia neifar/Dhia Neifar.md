#Home:
  Formé par des cartes des animaux, une vidéo et images Png clignotantes

#Pets:
  Formé par un grand nombre des cartes des animaux 

#Show:
  Une grande carte contenant tous les informations relatives à un animal

#Add:
  Un formulaire permettant d'introduire les informations relatives à un animal

#Update:
  Un formulaire permettant de modifier les informations relatives à un animal (les champs sont initialisés)

