
<body>
    <footer>
        
		<div class="bloc">
			<p><a href="/animals"><img src="/images/titlew.png" alt="AnimalShelter"></a></p>
			<p><br>© 2021 Animal Shelter.<br><br> All rights reserved.</p>
		</div>
		<div class="bloc">
			<p><span class="down">Get in Touch</span><br><br><br>Moonshine St.<br><br>14/05 Light City,<br><br>London, United Kingdom<br><br><br>
				<span>Email:</span> <a href="mailto:info@animalshelter.com">info@animalshelter.com</a><br><br><span>Phone:</span> <a href="tel://1234567920">+ 1235 2355 98</a></p>
		</div>
		<div class="bloc">
			<p>
				<span class="down">Learn More</span><br><br><br>
				<a href="/animals/about">About Us</a><br><br>
				<a href="#">Our Story</a><br><br>
				<a href="#">Projects</a><br><br>
				<a href="#">Terms of Use</a><br><br>
				<a href="#">Privacy Policy</a><br><br>
				<a href="/animals/contact">Contact Us</a>
			</p>
		</div>
		<div class="bloc">
			<p><span class="down">Our Newsletter</span><br><br><br>
			Subscribe to our newsletter to get <br>our news delivered to your inbox!</p>
			<form>
				<p>
					<input id="email" type="email" name="mail" id="mail" placeholder="Email Address">
					<input id="join" type="submit" value="JOIN">
				</p>
			</form>
		</div>
        
	</footer>
	<div id="bubble">
		<a href="#"><img src="/images/heart.png" width="20" title="Donate"></a>
		<a href="#"><img src="/images/adopt.png" width="20" title="Adopt"></a>
		<a href="#"><img src="/images/question.png" width="20" title="Support"></a>
	</div> 

    
    
</body>