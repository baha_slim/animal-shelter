<head>
	<title>Animal Shelter</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/css/navbar.css">
	<link rel="stylesheet" type="text/css" href="/css/footer.css">
	<link rel="shortcut icon" href="/images/logo.png">
	
</head>
<body>
	<header>
		<div id="logo">
			<a href="/animals"><img src="/images/title.png" alt="Animal Shelter"></a>
		</div>
		<nav>
			<ul id="menu">

				<li><a href="/animals">Home</a> </li>

				<li><a href="/animals/showall">Pets</a> </li>
                
        		<li><a href="/animals/create">Add</a></li>
				
				<li><a href="/animals/contact">Contact Us</a></li>

				<li><a href="/animals/about" >About</a></li>

				<li><form method="GET" action="{{action('AnimalController@search')}}">
					<input id="search" type="search" name="search" placeholder="search">
					<button type="search"><img src="/images/recherche.png" alt="search" width="15"></button></form></li>
					
				<li><a href="#"><img src="/images/info.png" alt="information" width="20" title="Search for a specific specie"></a></li>
			
			</ul>
		</nav>
	</header>
      @yield('content')
     </body>