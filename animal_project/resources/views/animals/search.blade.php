<head>
    <link rel="stylesheet" href="/css/search.css">
</head>

@extends('template.navbar')

@section('content')
<body>
    <div class="background">
        <div class="head">
        <div style="height: 100px"></div>
      
        <h1 style="margin-top: 150px;color:rgb(255,255,255);" >Welcome to our family</h1>
        <h4 style="text-align:center;margin-top: 50px;color:rgb(255,255,255);">Here we take care of pets <br> from different ages and species</h4>
        </div>
    </div>
    <div id="why_choose_us">
		<h4 style="text-transform:uppercase;">HERE ARE ALL THE {{$specie}}S </h4>
		<h1>Go ahead and and check them out</h1>
    </div>

    @php
    $i=0;  
@endphp
<div style="display: flex;flex-wrap: wrap;  width:1200px; margin:auto;margin-top:50px;">
@if(count($animals) > 0)
    @foreach ($animals as $animal)
    @php
    $i=$i+1;
    @endphp
    <br><br>
    <div class="container" style=" margin-top:50px; width:600px; padding:0px">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="box-shadow: 0px 0px 10px 1px #cccccc; ">
                    
                    <div class="card-header"> <h3 style="text-transform: capitalize;">{{$animal->name}}</h3> </div>
    
                    <div class="card-body" style="padding:50px 0px 0px 0px; ">
                        
                            @csrf

                            <div style="padding-bottom: 30px">
                                <p>@php
                                    $db = mysqli_connect("localhost","root","","animal"); 
                                    $sql = "SELECT * FROM animals WHERE id = $animal->id";
                                    $sth = $db->query($sql);
                                    $result=mysqli_fetch_array($sth);
                                    echo '<img height=185 width=275 style="border-radius: 4px; box-shadow: 0px 0px 10px 1px #cccccc;" src="/storage/animal/'.$result['file_path'].'"/>';

                                @endphp </p></div>
    
                            
                            <div class="form-group row" style="margin-left: 20;">
                                <label for="specie" class="col-md-4 col-form-label text-md-right">{{ __('Age :') }}</label>
    
                                <div class="col-md-6" style="padding-top: 5px;" >
                                    {{$animal->age}}
                                </div>
                                
                            </div>
                            <div class="form-group row" style="margin-left: 20;">
                                <label for="specie" class="col-md-4 col-form-label text-md-right">{{ __('Gender :') }}</label>
    
                                <div class="col-md-6" style="text-transform: capitalize; padding-top: 5px;">
                                    {{$animal->gender}}
                                </div>
                                
                            </div>
    
                            <h1 style="text-align: center;"><a type="submit" href="/animals/{{$animal->id}}" class="btn btn-primary btn-sm" >{{ __('Show Animal') }}</a></h1 >
                            
                            
                            <br>
                            <br>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    @endforeach
   
@else
    <h1 id="noanimal" style="padding-top: 90px;">No animals found</h1>
@endif
</div>
<br>
<br>


    @include('template.footer')
</body>
@endsection