<head>
    <link rel="stylesheet" href="/css/show.css">

</head>

@extends('template.navbar')
@section('content')
<body>
    <div class="background">
        <div class="head">
        <div style="height: 100px"></div>
      
        <h1 style="margin-top: 150px;color:rgb(255,255,255);" >Welcome to our family</h1>
        <h4 style="text-align:center;margin-top: 50px;color:rgb(255,255,255);">Here we take care of pets <br> from different ages and species</h4>
        </div>
    </div>
    <div id="why_choose_us">
		<h4>DID YOU LIKE <span style="text-transform: uppercase;">{{$theanimal->name}}</span>?</h4>
		<h1>Here are some more informations</h1>
    </div> 
<div class="container" style="margin-top: 50px;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="box-shadow: 0px 0px 10px 1px #cccccc; ">
                <div class="card-header"> <h3 style="text-transform: capitalize;">{{$theanimal->name}}</h3> </div>

                <div class="card-body" >
                    
                        @csrf

                        <div style="padding-top: 10px">
                            <p>
                            @php
                                $db = mysqli_connect("localhost","root","","animal"); 
                                $sql = "SELECT * FROM animals WHERE id = $theanimal->id";
                                $sth = $db->query($sql);
                                $result=mysqli_fetch_array($sth);
                                echo '<img id="output" src="/storage/animal/'.$result['file_path'].'"/>';

                            @endphp 
                            </p>
                        </div>

                        <div class="form-group row" style="margin-left: 20;">
                            <label for="specie" class="col-md-4 col-form-label text-md-right">{{ __('Specie :') }}</label>

                            <div class="col-md-6" style="text-transform: capitalize; padding-top: 5px;">
                                {{$theanimal->specie}}
                            </div>
                            
                        </div>
                        <div class="form-group row" style="margin-left: 20;">
                            <label for="age" class="col-md-4 col-form-label text-md-right">{{ __('Age :') }}</label>

                            <div class="col-md-6" style="text-transform: capitalize; padding-top: 5px;">
                                {{$theanimal->age}}
                            </div>
                            
                        </div>

                        <div class="form-group row" style="margin-left: 20;">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender :') }}</label>

                            <div class="col-md-6"  style="text-transform: capitalize; padding-top: 5px;">
                                {{$theanimal->gender}}
                            </div>
                            
                        </div>

                        <div class="form-group row" style="margin-left: 20;">
                            <label for="arrived_at" class="col-md-4 col-form-label text-md-right">{{ __('Arrived at :') }}</label>

                            <div class="col-md-6"  style=" padding-top: 5px;">
                                {{$theanimal->created_at}}
                            </div>
                            
                        </div>

                        <input type="hidden" id="idanimal" value="{{$theanimal->id}}" name="idanimal">
                        <div id="btns">
                        <h1 style="text-align: center;"><a type="submit" href="/animals/{{$theanimal->id}}/edit" class="btn btn-primary btn-sm" >Edit Animal</a></h1>
                        <h1 style="text-align: center;"><a type="submit" href="/animals/{{$theanimal->id}}/destroy" class="btn btn-primary btn-sm" onclick="return confirm('Are you sure you want to delete this animal?');">{{ __('Delete Animal') }}</a></h1>
                        </div>
                        <h1 style="text-align: center;"><a type="submit" href="/animals" class="btn btn-primary btn-sm">{{ __('Return to animals') }}</a></h1>
                    
                </div>
            </div>
        </div>
    </div>
</div> 

@include('template.footer')
</body>
@endsection


