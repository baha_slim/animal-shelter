<head>
    <link rel="stylesheet" href="/css/update.css">
</head>

@extends('template.navbar')

@section('content')
<body>
    <div class="background">
        <div class="head">
        <div style="height: 100px"></div>
      
        <h1 style="margin-top: 150px;color:rgb(255,255,255);" >Welcome to our family</h1>
        <h4 style="text-align:center;margin-top: 50px;color:rgb(255,255,255);">Here we take care of pets <br> from different ages and species</h4>
        </div>
    </div>
    <div id="why_choose_us">
		<h4>SOMETHING CAME UP ABOUT <span style="text-transform: uppercase;">{{$animal->name}}</span>?</h4>
		<h1>Go ahead and make necessary changes</h1>
    </div>

<div class="container" >
    <div class="row justify-content-center">
        <div class="col-md-8" >
            <div class="card" style="margin-top: 30px; box-shadow: 0px 0px 10px 1px #cccccc; ">
                <div class="card-header"><h4>{{ __("Update the animal's info") }}</h4></div>

                <div class="card-body">
                    <form method="POST" action="{{action('AnimalController@update',$animal->id)}}" enctype="multipart/form-data" >
                        @csrf
                        
                        <div style="margin-bottom: 40px;"><p><input type="file"  accept="image/*" name="file" id="file"  onchange="loadFile(event)" style="display: none;" ></p>
                            <p><label id="upload" for="file" style="cursor: pointer;">Upload Image</label></p>
                            <p>
                            @php
                                $db = mysqli_connect("localhost","root","","animal"); 
                                $sql = "SELECT * FROM animals WHERE id = $animal->id";
                                $sth = $db->query($sql);
                                $result=mysqli_fetch_array($sth);
                                echo '<img id="output"  src="/storage/animal/'.$result['file_path'].'"/>';
                            @endphp 
                            </p>
                            <script>
                                var loadFile = function(event) {
                                var image = document.getElementById('output');
                                image.src = URL.createObjectURL(event.target.files[0]);
                                };
                            </script>
                        </div>

                        <div class="form-group row" style="margin-right: 10px">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name :') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" value="{{$animal->name}}" class="form-control" name="name" required>
                            </div>
                        </div>
                        <div class="form-group row" style="margin-right: 10px">
                            <label for="specie" class="col-md-4 col-form-label text-md-right">{{ __('Specie :') }}</label>

                            <div class="col-md-6">
                                <input id="specie" type="text" value="{{$animal->specie}}" class="form-control" name="specie" required>
                            </div>
                        </div>

                        <div class="form-group row" style="margin-right: 10px">
                            <label for="age" class="col-md-4 col-form-label text-md-right">{{ __('Age :') }}</label>

                            <div class="col-md-6">
                                <input id="age" type="text" value="{{$animal->age}}" class="form-control" name="age" required>
                            </div>
                            
                        </div>

                        <div class="form-group row" style="margin-right: 10px">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender :') }}</label>

                            <div class="col-md-6">
                                <input id="gender" type="text" value="{{$animal->gender}}" class="form-control" name="gender" required>
                            </div>
                            
                        </div>

                        <div class="form-group row mb-0" style="justify-content: center;">
                            <div class="col-md-6 offset-md-4" style="margin:auto;">
                                <button type="submit" class="btn btn-primary" onclick=" return confirm('Confirm?');">
                                    {{ __('Update animal') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('template.footer')
</body>
@endsection


