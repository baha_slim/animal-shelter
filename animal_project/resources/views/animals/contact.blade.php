<head>
    <link rel="stylesheet" href="/css/contact.css" />
</head>

@extends('template.navbar')

@section('content')
<body>
    <div class="background">
        <div class="head">
        <div style="height: 100px"></div>
      
        <h1 style="margin-top: 150px;color:rgb(255,255,255);" >Welcome to our family</h1>
        <h4 style="text-align:center;margin-top: 50px;color:rgb(255,255,255);">Here we take care of pets <br> from different ages and species</h4>
        </div>
    </div>

<div id="container_1">
    <h4>GET IN TOUCH WITH US</h4>
	<h1>Come have a look or send us a message</h1>
    <div id="one_two_three">
        <div class="num1">
            <img src="/images/image_1.png" alt="img1" height="100" width="100">
            
            <p><span>Address:</span> 198 West 21th<br> Street, Suite 721 New York <br> NY 10016</p>
        </div>
        <div class="num1">
            <img src="/images/image_2.png" alt="img2"  height="100" width="100">
            
            <p><span>Phone:</span> <a href="tel://1234567920">+ 1235 2355 98</a></p>
        </div>
        <div class="num1">
            <img src="/images/image_3.png" alt="img3"  height="100" width="100">
            
            <p><span>Email:</span> <a href="mailto:info@animalshelter.com">info@animalshelter.com</a></p>
        </div>
        <div class="num1" style="padding-top: 13px">
            <img src="/images/image_4.png" alt="img4"  height="70" width="70" style="margin-bottom: 15px">
            
            <p><span>Website:</span> <a href="/animals">animalshelter.com</a></p>
        </div>
    </div>
    <div id="container_1_1">
        <div>
            <h4>Our Location</h4>
            <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d51054.92154701359!2d10.152751960714367!3d36.89196070777949!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x12e2cb6fc49b7883%3A0x84da64ea383c01d2!2sSup&#39;Com!3m2!1d36.891897199999995!2d10.1877715!5e0!3m2!1sfr!2stn!4v1638207329043!5m2!1sfr!2stn" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
        <div>
            <h4>Contact Us</h4>
            <div id="sondage">
                <div class="contact-wrap w-100 p-md-5 p-4">
                    <form method="POST" id="contactForm" name="contactForm" class="contactForm"  action="{{action('AnimalController@mail')}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="label" for="name">Full Name</label>
                                    <input type="text" class="form-control error" name="name"  placeholder="Name" aria-invalid="true" required>
                                </div>
                            </div>
                            <div class="col-md-6"> 
                                <div class="form-group">
                                    <label class="label" for="email">Email Address</label>
                                    <input type="email" class="form-control error" name="email"  placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="label" for="subject">Subject</label>
                                    <input type="text" class="form-control error" name="subject"  placeholder="Subject" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="label" for="#">Message</label>
                                    <textarea name="message" class="form-control error"  cols="30" rows="4" placeholder="Message" required></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="submit" value="Send Message" class="btn btn-primary">
                                   
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>		
        
    </div>
</div>


@include('template.footer')
</body>
@endsection
