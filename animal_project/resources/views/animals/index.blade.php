<head>
    <link rel="stylesheet" type="text/css" href="/css/index.css">
</head>

@extends('template.navbar')
@section('content')
<body>
<div id="banniere_image">
    <center>
    <div id="banniere_description">  
        <h1>“Animals are such agreeable friends. <br>They ask no questions,<br>they pass no criticisms.”</h1>
        <h4>George Eliot</h4>
        <p>AnimalShelter is now available to download from<br>both the App Store and Google Play Store.</p>
        <div id="button"> 
            <a href="#" class="app_store"><img src="images/apple.png" alt="" width="15" />APP STORE</a>
            <a href="#" class="google_play"><img src="images/store.png" alt=""
            width="15" />GOOGLE PLAY</a>
        </div>
        <div id="anime">
        <img src="images/anime2.png" alt="dog" width="300" height="400">
        </div>
    </div>
    </center>
</div>

<div id="anim">
    <h5>FIND YOUR NEW FRIEND</h5>
    <h1>Dogs, Cats, Birds, Rabbits <br>And a lot more....</h1>
</div>


@php
    $i=0;  
@endphp
<div style="display: flex;flex-wrap: wrap;  width:1200px; margin:auto;margin-top:50px;">
@if(count($animals) > 0)
    @foreach ($animals as $animal)
    @php
    $i=$i+1;
    @endphp
    <br><br>
    <div class="container" style=" margin-top:50px; width:600px; padding:0px">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="box-shadow: 0px 0px 10px 1px #cccccc; ">
                    
                    <div class="card-header"> <h3 style="text-transform: capitalize;">{{$animal->name}}</h3> </div>
    
                    <div class="card-body" style="padding:50px 0px 0px 0px; ">
                        
                            @csrf

                            <div style="padding-bottom: 30px">
                                <p>@php
                                    $db = mysqli_connect("localhost","root","","animal"); 
                                    $sql = "SELECT file_path FROM animals WHERE id = $animal->id";
                                    $sth = $db->query($sql);
                                    $result=mysqli_fetch_array($sth);
                                    echo '<img height=185 width=275 style="border-radius: 4px; box-shadow: 0px 0px 10px 1px #cccccc;" src="/storage/animal/'.$result['file_path'].'"/>';
                                @endphp </p></div>
                            
                            <div class="form-group row" style="margin-left: 20;">
                                <label for="specie" class="col-md-4 col-form-label text-md-right">{{ __('Age :') }}</label>
    
                                <div class="col-md-6" style="padding-top: 5px;" >
                                    {{$animal->age}}
                                </div>
                                
                            </div>
                            <div class="form-group row" style="margin-left: 20;">
                                <label for="specie" class="col-md-4 col-form-label text-md-right">{{ __('Gender :') }}</label>
    
                                <div class="col-md-6" style="text-transform: capitalize; padding-top: 5px;">
                                    {{$animal->gender}}
                                </div>
                                
                            </div>
    
                            <h1 style="text-align: center;"><a type="submit" href="/animals/{{$animal->id}}" class="btn btn-primary btn-sm" >{{ __('Show Animal') }}</a></h1 >
                            
                            
                            <br>
                            <br>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    @endforeach
   
@else
    <h1 id="noanimal" style="padding-top: 90px;">No animals found</h1>
@endif
</div>
<br>
<br>

<div id="closer">
	<h5>HAVE A CLOSER LOOK</h5>
	<h1>We take care of each other,<br>
		We care for each other.</h1>
	<p><video src="images/vid.mp4" controls poster="images/vid.jpg"  width="983" height="552"></video></p>
</div>

<div id="partners">
    <h5>PARTNERS</h5>
    <h1>Together we grow bigger</h1>
    <p id="img"><img src="images/z1.svg" width="150"><img src="images/z2.svg" width="150"><img src="images/z3.svg" width="150"><img src="images/z4.svg" width="150"><img src="images/z5.svg" width="150"><img src="images/z6.svg" width="150"><img src="images/z7.svg" width="150"><img src="images/z8.svg" width="150"></p>
</div>

@include('template.footer')
</body>
@endsection



