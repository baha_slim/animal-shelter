<head>
    <link rel="stylesheet" href="/css/about.css" />
</head>

@extends('template.navbar')

@section('content')

<body>
    <div class="background">
        <div class="head">
        <div style="height: 100px"></div>
      
        <h1 style="margin-top: 150px;color:rgb(255,255,255);" >Welcome to our family</h1>
        <h4 style="text-align:center;margin-top: 50px;color:rgb(255,255,255);">Here we take care of pets <br> from different ages and species</h4>
        </div>
    </div>
    <div id="why_choose_us">
		<h4>WHO ARE WE?</h4>
		<h1>Here we take care of animals<br>
		a shelter, a safe place, A HOME...</h1>

		<div class="img_text">
			
			<div class="text">
				<h2>Search for Lost Animals</h2>
				<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
				<br>
				<h2>Provide Warm, Food, Traitment</h2>
				<p>Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue. Maecenas faucibus mollis interdum.</p>
				<br>
				<h2>Look for Adopters</h2>
				<p>Pellentesque ornare sem lacinia quam venenatis vestibulum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
			</div>
            <div class="img"><img src="/images/img7.jpg" alt="animals" width="500" height="450"></div>
		</div>
	</div>
    <div id="why_choose_us">
		<h4>WHY CHOOSE US ?</h4>
		<h1>Here are a few reasons why our<br>
		customers choose Animal Shelter.</h1>
		
		<div class="img_text">
			<div class="img"><img src="/images/img5.jpg" alt="animals" width="500" height="450"></div>
			<div class="text">
				<h2>Organised & Scheduled Visits</h2>
				<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
				<br>
				<h2>Reports & Following</h2>
				<p>Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue. Maecenas faucibus mollis interdum.</p>
				<br>
				<h2>Free Animal Accessoiries</h2>
				<p>Pellentesque ornare sem lacinia quam venenatis vestibulum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
			</div>
		</div>
		
		<div class="img_text">
			<div class="text">
				<h2>Clean and Tidy Holds</h2>
				<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
				<br>
				<h2>Active and Friendly Animals</h2>
				<p>Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue. Maecenas faucibus mollis interdum.</p>
				<br>
				<h2>Vaccinated Animals</h2>
				<p>Pellentesque ornare sem lacinia quam venenatis vestibulum. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
			</div>
			<div class="img"><img src="/images/img6.jpg" alt="animals" width="500" height="450"></div>
		</div>

	</div>
    
        <div id="prog_bar">
            <h4>SOME NUMBERS</h4>
            <h1>Animals that have been adopted.</h1>
            <div id="sondage">
                <div>
                    <div class="caption"><p>Dogs</p><p>90%</p></div>
                    <div class="gray_bar"><div id="colored_bar1"></div></div>
                </div>
                <div>
                    <div class="caption"><p>Cats</p><p>80%</p></div>
                    <div class="gray_bar"><div id="colored_bar2"></div></div>
                </div>
                <div>
                    <div class="caption"><p>Rabbits</p><p>85%</p></div>
                    <div class="gray_bar"><div id="colored_bar3"></div></div>
                </div>
                <div>
                    <div class="caption"><p>Birds</p><p>65%</p></div>
                    <div class="gray_bar"><div id="colored_bar4"></div></div>
                </div>
                <div>
                    <div class="caption"><p>Others</p><p>50%</p></div>
                    <div class="gray_bar"><div id="colored_bar5"></div></div>
                </div>

            </div>

        </div>
   
    @include('template.footer')
</body>

@endsection