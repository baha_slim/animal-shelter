<head>
    <link rel="stylesheet" href="/css/create.css">
</head>

@extends('template.navbar')

@section('content')
<body>
    <div class="background">
        <div class="head">
        <div style="height: 100px"></div>
      
        <h1 style="margin-top: 150px;color:rgb(255,255,255);" >Welcome to our family</h1>
        <h4 style="text-align:center;margin-top: 50px;color:rgb(255,255,255);">Here we take care of pets <br> from different ages and species</h4>
        </div>
    </div>
    <div id="why_choose_us">
		<h4>ADDING ANOTHER MEMBER? </h4>
		<h1>Our family just keep getting bigger and bigger</h1>
    </div>
    

<div class="container" style="margin-top: 50px; ">
    <div class="row justify-age-center" style="width: 1150px">
        <div class="col-md-8" style="margin-left: 16%">
            <div class="card" style="box-shadow: 0px 0px 10px 1px #cccccc; ">
                <div class="card-header"><h4>{{ __('Put in the animals info') }}</h4></div>

                <div class="card-body">
                    <form method="POST" action="{{action('AnimalController@store')}}" enctype="multipart/form-data">
                        @csrf

                        <div style="padding-bottom: 30px"><p><input type="file"  accept="image/*" name="file" id="file"  onchange="loadFile(event)" style="display: none;" required></p>
                            <p><label id="upload" for="file" style="cursor: pointer;">Upload Image</label></p>
                            <p><img id="output" style="" /></p>
                            
                            <script>
                            var loadFile = function(event) {
                                var image = document.getElementById('output');
                                image.src = URL.createObjectURL(event.target.files[0]);
                            };
                            </script>
                        </div>

                        <div class="form-group row" style="margin-right: 10px">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name :') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required>
                            </div>
                        </div>
                        <div class="form-group row" style="margin-right: 10px">
                            <label for="age" class="col-md-4 col-form-label text-md-right">{{ __('Age :') }}</label>

                            <div class="col-md-6">
                                <input id="age" type="text" class="form-control" name="age" required>
                            </div>
                            
                        </div>
                        <div class="form-group row" style="margin-right: 10px">
                            <label for="field" class="col-md-4 col-form-label text-md-right">{{ __('Specie :') }}</label>

                            <div class="col-md-6">
                                <input id="specie" type="text" class="form-control" name="specie" required>
                            </div>
                            
                        </div>

                        <div class="form-group row" style="margin-right: 10px">
                            <label for="field" class="col-md-4 col-form-label text-md-right">{{ __('Gendre :') }}</label>

                            <div class="col-md-6">
                                <input id="gender" type="text" class="form-control" name="gender" required>
                            </div>
                            
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4" style="margin:auto;">
                                <button type="submit" class="btn btn-primary"  >
                                    {{ __('Add new animal') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('template.footer')
</body>
@endsection