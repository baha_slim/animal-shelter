<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/animals/showall','AnimalController@showall');
Route::get('/animals/about','AnimalController@about');
Route::get('/animals/contact','AnimalController@contact');
Route::post('/animals/contact','AnimalController@mail');

Route::get('/animals/search', 'AnimalController@search');
Route::resource('/animals', 'AnimalController', ['except' => ['destroy','update']]);
Route::post('/animals/{animal}/update','AnimalController@update');
Route::get('/animals/{animal}/destroy','AnimalController@destroy');


