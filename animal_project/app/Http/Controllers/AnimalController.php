<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Animal;
use Illuminate\Support\Facades\DB;

class AnimalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $animals = animal::paginate(4);
        return view('animals.index')->with('animals',$animals);
    }

    public function showall()
    {
       
        $allanimals = animal::paginate(10);
        return view('animals.showall')->with('allanimals',$allanimals);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('animals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'age' => 'required',
            'specie' => 'required',
            'gender' => 'required',
        ]);
        try{
            $animal = new animal();
            $animal->name = $request->name; 
            $animal->specie = $request->specie; 
            $animal->age = $request->age; 
            if(!empty($request->gender)){  
                $animal->gender = $request->gender;
            }
            if ($request->hasFile('file')) {

                $request->file->store('animal', 'public');
                $animal->file_path = $request->file->hashName();
            }
            $animal->save();
        }catch(exception $e){
            $e->getMessage();
        }
        return redirect()->to('/animals');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $theanimal = animal::where('id',$id)->get()[0]; 
        return view('animals.show',compact('theanimal'));
    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $animal = animal::find($id); //find comme where
        return view('animals.update')->with('animal',$animal);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request,['name'=>'required']);
            $animal = animal::find($id);
            $animal->name = $request->name; 
            $animal->specie = $request->specie; 
            $animal->age = $request->age; 
            if(!empty($request->gender)){  
                $animal->gender = $request->gender;
            }
            if ($request->hasFile('file')) {
                $request->file->store('animal', 'public');
                $animal->file_path = $request->file->hashName();
            }
            $animal->update();
        }catch(exception $e){
            $e->getMessage();
        }
        return redirect()->to('/animals');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $animal = animal::find($id);
        $animal->delete();
        return redirect()->to('/animals');
    }


    public function about(){
        return view('animals.about');
    }

    public function contact(){
        return view('animals.contact');
    }

    
    public function search(Request $request){
        $animals = animal::where('specie',$request->search)->get();
        $specie = $request->search;
        return view('animals.search', compact('animals','specie'));
     }
    

    public function mail(Request $request){
        if ($request->message) {
            $position_arobase = strpos($request->email, '@');
            if ($position_arobase === false)
                echo '<p>Votre email doit comporter un arobase.</p>';
            else {
                $retour = mail('bahaeddine.slim@supcom.tn', $request->subject, $request->message, 'From: ' . $request->email);
                if($retour)
                    echo '<p>Votre message a été envoyé.</p>';
                else
                    echo '<p>Erreur.</p>';
            }
        }
        return redirect()->to('/animals/contact');
    }



     
     
     
}
