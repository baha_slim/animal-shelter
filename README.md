# Animal Shelter
An interactive web site where user can:


See the animals available in a shelter && info about the shelter( home && pets && about)
	
Make contact with the shelter by having a phone call or sending email or filling the contact us form(will automatically send an email with the info introduced by the user {doesn't work on a local host})(contact Us && footer)

Find the shelter location on the map
	
Add new animals (add),edit animals info and delete animals (after clicking on show animal on the animal's card)
	
Make a search for a specific specie (dog, cat, snake, rabbit, bird, fish)

# Data Base info:
DB_DATABASE=animal

DB_USERNAME=root

DB_PASSWORD=

# About
The project was developped on Laravel framework (fullstack)