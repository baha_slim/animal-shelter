#Division des tàches

  Dans un site web plusieurs éléments se répètent dans les différentes pages du site. Pour cela,
On a choisi de diviser les taches selon les éléments du site (Header, Footer, Navbar...).
Autrement dit, on a attribué à chaque membre un ensemble d'éléments à réaliser.
Ensuite ces éléments seront assemblés et adaptés à l'environnement Laravel tout en déffinissant
les élement du backend (Controllers, Models, Migrations..) et ses différentes 
fonctionnalités (appels get && post avec la base des donnés...).